#include <cstdlib>
#include <random>
#include <vector>
#include <chrono>
#include <fstream>
#include <algorithm>
#include <iostream>
#include <omp.h>
#include "board.h"
#include "move.h"
#include "bag.h"
#define forstate for(int i = 0 ; i < 4 ; i++) for(int j = 0 ; j < 4 ; j++) for(int k = 0 ; k < 4 ; k++)
typedef unsigned long long ULL;
std::default_random_engine engine;
std::vector<Move> record;
extern Index *Tuple[4][4][4];
extern ULL num_Tuple[4][4][4];
int location[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};
int num[] = {1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3};
int next[4][4] = {{12, 13, 14, 15}, {0, 4, 8, 12}, {0, 1, 2, 3}, {3, 7, 11, 15}};
float worst = 0, best = 0;
//int search_level = 0;
float ab(Board &board, const int next_tile, const int dir, float a, float b, const int d, Bag bag)
{
    int maxtile = board.maxtile();
    bag.use(next_tile);
    bag.reset_max(maxtile);
    for(int i = 1 ; i < std::max(4, maxtile - 2) ; i++)
    {
        if(!bag.rest_tile(i)) continue;
        #pragma omp parallel for num_threads(4)
        for(int j = 0 ; j < 4 ; j++)
        {
            Board tmp = board, tmp2;
            float value, maxa = a;
            if(!tmp.take_place(next[dir][j], next_tile)) continue;
            for(int k = 0 ; k < 4 ; k++)
            {
                tmp2 = tmp;
                if(tmp2.slide(k) == -1) continue;
                if(!d) value = tmp2.get_tuple(NULL, i > 3 ? 3 : i - 1, k);
                else value = ab(tmp2, i, k, maxa, b, d-1, bag);
                maxa = std::max(maxa, value);
                if(maxa >= b) break;
            }
            #pragma omp critical
            {
                b = std::min(maxa, b);
            }
        }
        if(a >= b) return b;
    }
    return b;
}
float search(Board &board, const Bag &bag, int &next_tile)
{
    int dir, maxtile = board.maxtile();
    float minvalue, maxvalue;
    next_tile = -1;
    for(int i = 1 ; i < std::max(maxtile - 2, 4) ; i++)
    {
        if(!bag.rest_tile(i)) continue;
        dir = -1;
        #pragma omp parallel for num_threads(4)
        for(int j = 0 ; j < 4 ; j++)
        {
            Board tmp = board;
            if(tmp.slide(j) != -1)
            {
                float value;
                if(tmp.maxtile() > 7) value = ab(tmp, i, j, worst * 32, best * 32, 1, bag);
                else  value = ab(tmp, i, j, worst * 32, best * 32, 0, bag);
                //else value = tmp.get_tuple(NULL, i > 3 ? 3 : i - 1, j);
                #pragma omp critical
                if(dir == -1 || value > maxvalue)
                {
                    dir = j;
                    maxvalue = value;
                }
            }
        }
        if(dir == -1)
        {
            next_tile = i;
            return worst * 32;
        }
        if(next_tile == -1 || maxvalue < minvalue)
        {
            next_tile = i;
            minvalue = maxvalue;
        }
    }
    return minvalue;
}
void Computer(Board &board, const int dir, int &next_tile, Bag &bag)
{
    if(dir == 4)
    {
        int s = bag.total();
        if(!s)
        {
            shuffle(num, num + 12, engine);
            shuffle(location, location + 16, engine);
        }
        board.take_place(location[s], num[s]); 
        record.push_back(Move(location[s], num[s]));
        bag.use(num[s]);
        if(s == 8) search(board, bag, next_tile);
        else next_tile = num[s+1];
    }
    else
    {
        bag.reset_max(board.maxtile());
        int place = -1, tile = next_tile;
        float minvalue;
        #pragma omp parallel for num_threads(4)
        for(int i = 0 ; i < 4 ; i++)
        {
            Board tmp_board = board;
            if(tmp_board.take_place(::next[dir][i], tile))
            {
                Bag tmp_bag = bag;
                tmp_bag.use(tile);
                int tmp_tile;
                float value = search(tmp_board, tmp_bag, tmp_tile);
                #pragma omp critical
                if(place == -1 || value < minvalue)
                {
                    place = next[dir][i];
                    next_tile = tmp_tile;
                    minvalue = value;
                }
            } 
        }
        board.take_place(place, tile);
        record.push_back(Move(place, tile));
        bag.use(tile);
    }
}
void input()
{
    unsigned index;
    ULL cnt = 0, num;
    float value;
    std::ifstream fin("data", std::ios::binary);
    forstate
    {
        fin.read((char*)&num, sizeof(ULL));
        cnt += num;
        num_Tuple[i][j][k] = num;
        Tuple[i][j][k] = new Index[num];
        while(num--)
        {
            fin.read((char*)&index, sizeof(unsigned));
            fin.read((char*)&value, sizeof(float));
        	Tuple[i][j][k][num].index = index;
        	Tuple[i][j][k][num].value = value;
            worst = std::min(worst, value);
            best = std::max(best, value);
        }
    }
	fin.close();
	forstate std::sort(Tuple[i][j][k], Tuple[i][j][k] + num_Tuple[i][j][k]);
	std::cout << "The number of Tuple : " << cnt << std::endl;
}
time_t get_time()
{
    auto now = std::chrono::system_clock::now().time_since_epoch();
    return std::chrono::duration_cast<std::chrono::milliseconds>(now).count();
}
int main(int n, char **argv)
{
    std::ios_base::sync_with_stdio(0);
    std::cin.tie(0);
    int next_tile, dir;
    Board board;
    Bag bag;
    time_t time_start, time_end;
    char command[100], id[10];
    bool playing = 0;
    //if(n > 1) search_level = atoi(argv[0]);

    engine.seed(time(NULL));
    std::cout << "Start Reading." << std::endl;
    input();
    std::cout << "Reading Complete." << std::endl;
    std::cout << "Start Playing." << std::endl;
    std::ofstream fout("result.txt"), fsp("sp.txt");
    while(std::cin >> id)
    {
        if(id[0] == '#')
        {
            std::cin >> command;
            if(command[0] == '?') continue;
            else if(!std::strcmp(command, "open"))
            {
                std::cin >> command;
                if(!playing)
                {
                    std::cout << id << " open accept" << std::endl;
                    dir = 4;
                    board = Board();
                    bag.init();
                    record.clear();
                    playing = true;
                    while(std::cin >> command && command[0] != '?');
                    for(int i = 0 ; i < 9 ; i++)
                    {
                        Computer(board, dir, next_tile, bag);
                        std::cout << id << " " << record.back() << "+" << (next_tile > 3 ? 4 : next_tile) << std::endl;
                    }
                    time_start = get_time();
                }
                else std::cout << id << " open reject" << std::endl;
            }
            else if(!std::strcmp(command, "close"))
            {
                playing = false;
                time_end = get_time();
                fout << "0516310:random_envil@" << time_start << "|";
                for(int j = 0 ; j < record.size() ; j++) fout << record[j];
                fout << "|random_envil@" << time_end << "\n";
                std::cin >> command;
                fsp << command << "\n";
            }
            else if(command[0] == '#')
            {
                switch(command[1])
                {
                    case 'U' : board.slide(dir = 0); break;
                    case 'D' : board.slide(dir = 2); break;
                    case 'L' : board.slide(dir = 3); break;
                    case 'R' : board.slide(dir = 1); break;
                    default : std::cout << "Unrecognized command : " << id << " " << command << std::endl;
                }
                record.push_back(Move(dir));
                Computer(board, dir, next_tile, bag);
                std::cout << id << " " << record.back() << "+" << (next_tile > 3 ? 4 : next_tile) << std::endl;
            }
            else std::cout << "Unrecognized command : " << id << " " << command << std::endl;
        }
        else if(id[0] == '@')
        {
            std::cin >> command;
            if(!std::strcmp(command, "login")) std::cout << "@ login 0516310|c0AtLrDli9" << std::endl;
            else if(!std::strcmp(command, "logout")) break;
            else std::cout << "Unrecognized command : " << id << " " << command << std::endl;
        }
        //else std::cout << "Unrecognized command : " << id << " " << command << std::endl;
        
    }
    fout.close();
    fsp.close();
    std::cout << "Complete playing." << std::endl;
    return 0;
}
