#pragma once
#include <algorithm>
#include <iostream>
#include <cstring>
#include <unordered_map>
#include <omp.h>
#define for_i(x) for(int i = 0 ; i < (x) ; i++)
#define for_j(x) for(int j = 0 ; j < (x) ; j++)
#define GET(n) (board & (15ULL << ((n) << 2))) //return value without shifting back
#define NUM(n) (GET(n) >> ((n) << 2)) //return value after shifting back
#define SET(n, x) board &= ~(15ULL << ((n) << 2)); board |= ((x) << ((n) << 2));
typedef std::unordered_map<unsigned, float> State;
typedef unsigned long long ULL;
class Index
{
public:
	unsigned index;
	float value;
	bool operator < (const Index &s) const {return index < s.index;}
	bool operator < (const unsigned &s) const {return index < s;}
};
Index *Tuple[4][4][4];
ULL num_Tuple[4][4][4];
class Board
{
private:
	ULL board; 
	/* 
		board index: 
		15 14 13 12
		11 10 09 08
		07 06 05 04
		03 02 01 00
	*/
public:
	Board() {board = 0;}
	bool take_place(unsigned n, unsigned num) // put tile or return false
	{
		if(GET(n)) return false;
		SET(n, (ULL)num);
		return true;
	}
	unsigned maxtile()
	{
		ULL ans = 0;
		for_i(16) ans = std::max(ans, GET(i) >> (i << 2));
		return ans;
	}
	unsigned count_score()
	{
		unsigned score = 0, tmp;
		for_i(16)
		{
			tmp = GET(i) >> (i << 2);
			if(tmp > 2) score += std::pow(3, tmp - 3);
		}
		return score;
	}
	bool slide_up(ULL &score)
	{
		bool change = 0;
		unsigned square[16], tmp;
		score = board;
		for(int i = 0 ; i < 16 ; i++) {square[i] = score & 15; score >>= 4;}
		score = 0;
		for(int i = 0 ; i < 4 ; i++)
		{
			tmp = i;
			for(int j = 0 ; j < 3 ; j++)
			{
				tmp += 4;
				if(!square[tmp]) continue;
				if(!square[tmp - 4])
				{
					square[tmp - 4] = square[tmp];
					square[tmp] = 0;
					change = 1;
				}
				else if(square[tmp] + square[tmp - 4] == 3)
				{
					score++;
					square[tmp - 4] = 3;
					square[tmp] = 0;
					change = 1;
				}
				else if(square[tmp] == square[tmp - 4] && square[tmp] > 2)
				{
					score += std::pow(3, square[tmp] - 2);
					square[tmp - 4]++;
					square[tmp] = 0;
					change = 1;
				}
			}
		}
		board = 0;
		for(int i = 15 ; i > -1 ; i--) {board <<= 4; board |= square[i];}
		return change;
	}
	bool slide_down(ULL &score)
	{
		bool change = 0;
		unsigned square[16], tmp;
		score = board;
		for(int i = 0 ; i < 16 ; i++) {square[i] = score & 15; score >>= 4;}
		score = 0;
		for(int i = 12 ; i < 16 ; i++)
		{
			tmp = i;
			for(int j = 0 ; j < 3 ; j++)
			{
				tmp -= 4;
				if(!square[tmp]) continue;
				if(!square[tmp + 4])
				{
					square[tmp + 4] = square[tmp];
					square[tmp] = 0;
					change = 1;
				}
				else if(square[tmp] + square[tmp + 4] == 3)
				{
					score++;
					square[tmp + 4] = 3;
					square[tmp] = 0;
					change = 1;
				}
				else if(square[tmp] == square[tmp + 4] && square[tmp] > 2)
				{
					score += std::pow(3, square[tmp] - 2);
					square[tmp + 4]++;
					square[tmp] = 0;
					change = 1;
				}
			}
		}
		board = 0;
		for(int i = 15 ; i > -1 ; i--) {board <<= 4; board |= square[i];}
		return change;
	}
	bool slide_right(ULL &score)
	{
		bool change = 0;
		unsigned square[16], tmp;
		score = board;
		for(int i = 0 ; i < 16 ; i++) {square[i] = score & 15; score >>= 4;}
		score = 0;
		for(int i = 0 ; i < 4 ; i++)
		{
			tmp = 3 + (i << 2);
			for(int j = 0 ; j < 3 ; j++)
			{
				tmp--;
				if(!square[tmp]) continue;
				if(!square[tmp + 1])
				{
					square[tmp + 1] = square[tmp];
					square[tmp] = 0;
					change = 1;
				}
				else if(square[tmp] + square[tmp + 1] == 3)
				{
					score++;
					square[tmp + 1] = 3;
					square[tmp] = 0;
					change = 1;
				}
				else if(square[tmp] == square[tmp + 1] && square[tmp] > 2)
				{
					score += std::pow(3, square[tmp] - 2);
					square[tmp + 1]++;
					square[tmp] = 0;
					change = 1;
				}
			}
		}
		board = 0;
		for(int i = 15 ; i > -1 ; i--) {board <<= 4; board |= square[i];}
		return change;
	}
	bool slide_left(ULL &score)
	{
		bool change = 0;
		unsigned square[16], tmp;
		score = board;
		for(int i = 0 ; i < 16 ; i++) {square[i] = score & 15; score >>= 4;}
		score = 0;
		for(int i = 0 ; i < 4 ; i++)
		{
			tmp = i << 2;
			for(int j = 0 ; j < 3 ; j++)
			{
				tmp++;
				if(!square[tmp]) continue;
				if(!square[tmp - 1])
				{
					square[tmp - 1] = square[tmp];
					square[tmp] = 0;
					change = 1;
				}
				else if(square[tmp] + square[tmp - 1] == 3)
				{
					score++;
					square[tmp - 1] = 3;
					square[tmp] = 0;
					change = 1;
				}
				else if(square[tmp] == square[tmp - 1] && square[tmp] > 2)
				{
					score += std::pow(3, square[tmp] - 2);
					square[tmp - 1]++;
					square[tmp] = 0;
					change = 1;
				}
			}
		}
		board = 0;
		for(int i = 15 ; i > -1 ; i--) {board <<= 4; board |= square[i];}
		return change;
	}
	int slide(int dir)
	{
		bool canmove;
		ULL score;
		switch(dir)
		{
			case 0: canmove = slide_up(score); break;
			case 1: canmove = slide_right(score); break;
			case 2: canmove = slide_down(score); break;
			case 3: canmove = slide_left(score); break;
			default: return -1;
		}
		if(canmove) return (int)score;
		return -1;
	}
	void tuple_index(unsigned *index, int kase)
	{
		switch(kase)
		{
			case 0:
				index[0] = ((((NUM(0) << 5 | NUM(1)) << 5 | NUM(2)) << 5 | NUM(4)) << 5 | NUM(6)) << 5 | NUM(10);
				index[1] = ((((NUM(0) << 5 | NUM(1)) << 5 | NUM(5)) << 5 | NUM(9)) << 5 | NUM(13)) << 5 | NUM(14);
				index[2] = ((((NUM(2) << 5 | NUM(3)) << 5 | NUM(6)) << 5 | NUM(10)) << 5 | NUM(11)) << 5 | NUM(14);
				index[3] = ((((NUM(8) << 5 | NUM(11)) << 5 | NUM(12)) << 5 | NUM(13)) << 5 | NUM(14)) << 5 | NUM(15);
				break;
			case 1:
				index[0] = ((((NUM(3) << 5 | NUM(7)) << 5 | NUM(11)) << 5 | NUM(2)) << 5 | NUM(10)) << 5 | NUM(9);
				index[1] = ((((NUM(3) << 5 | NUM(7)) << 5 | NUM(6)) << 5 | NUM(5)) << 5 | NUM(4)) << 5 | NUM(8);
				index[2] = ((((NUM(11) << 5 | NUM(15)) << 5 | NUM(10)) << 5 | NUM(9)) << 5 | NUM(13)) << 5 | NUM(8);
				index[3] = ((((NUM(1) << 5 | NUM(13)) << 5 | NUM(0)) << 5 | NUM(4)) << 5 | NUM(8)) << 5 | NUM(12);
				break;
			case 2:
				index[0] = ((((NUM(15) << 5 | NUM(14)) << 5 | NUM(13)) << 5 | NUM(11)) << 5 | NUM(9)) << 5 | NUM(5);
				index[1] = ((((NUM(15) << 5 | NUM(14)) << 5 | NUM(10)) << 5 | NUM(6)) << 5 | NUM(2)) << 5 | NUM(1);
				index[2] = ((((NUM(13) << 5 | NUM(12)) << 5 | NUM(9)) << 5 | NUM(5)) << 5 | NUM(4)) << 5 | NUM(1);
				index[3] = ((((NUM(7) << 5 | NUM(4)) << 5 | NUM(3)) << 5 | NUM(2)) << 5 | NUM(1)) << 5 | NUM(0);
				break;
			case 3:
				index[0] = ((((NUM(12) << 5 | NUM(8)) << 5 | NUM(4)) << 5 | NUM(13)) << 5 | NUM(5)) << 5 | NUM(6);
				index[1] = ((((NUM(12) << 5 | NUM(8)) << 5 | NUM(9)) << 5 | NUM(10)) << 5 | NUM(11)) << 5 | NUM(7);
				index[2] = ((((NUM(4) << 5 | NUM(0)) << 5 | NUM(5)) << 5 | NUM(6)) << 5 | NUM(2)) << 5 | NUM(7);
				index[3] = ((((NUM(14) << 5 | NUM(2)) << 5 | NUM(15)) << 5 | NUM(11)) << 5 | NUM(7)) << 5 | NUM(3);
				break;
			case 4:
				index[0] = ((((NUM(3) << 5 | NUM(2)) << 5 | NUM(1)) << 5 | NUM(7)) << 5 | NUM(5)) << 5 | NUM(9);
				index[1] = ((((NUM(3) << 5 | NUM(2)) << 5 | NUM(6)) << 5 | NUM(10)) << 5 | NUM(14)) << 5 | NUM(13);
				index[2] = ((((NUM(1) << 5 | NUM(0)) << 5 | NUM(5)) << 5 | NUM(9)) << 5 | NUM(8)) << 5 | NUM(13);
				index[3] = ((((NUM(11) << 5 | NUM(8)) << 5 | NUM(15)) << 5 | NUM(14)) << 5 | NUM(13)) << 5 | NUM(12);
				break;
			case 5:
				index[0] = ((((NUM(15) << 5 | NUM(11)) << 5 | NUM(7)) << 5 | NUM(14)) << 5 | NUM(6)) << 5 | NUM(5);
				index[1] = ((((NUM(15) << 5 | NUM(11)) << 5 | NUM(10)) << 5 | NUM(9)) << 5 | NUM(8)) << 5 | NUM(4);
				index[2] = ((((NUM(7) << 5 | NUM(3)) << 5 | NUM(6)) << 5 | NUM(5)) << 5 | NUM(1)) << 5 | NUM(4);
				index[3] = ((((NUM(13) << 5 | NUM(1)) << 5 | NUM(12)) << 5 | NUM(8)) << 5 | NUM(4)) << 5 | NUM(0);
				break;
			case 6:
				index[0] = ((((NUM(12) << 5 | NUM(13)) << 5 | NUM(14)) << 5 | NUM(8)) << 5 | NUM(10)) << 5 | NUM(6);
				index[1] = ((((NUM(12) << 5 | NUM(13)) << 5 | NUM(9)) << 5 | NUM(5)) << 5 | NUM(1)) << 5 | NUM(2);
				index[2] = ((((NUM(14) << 5 | NUM(15)) << 5 | NUM(10)) << 5 | NUM(6)) << 5 | NUM(7)) << 5 | NUM(2);
				index[3] = ((((NUM(4) << 5 | NUM(7)) << 5 | NUM(0)) << 5 | NUM(1)) << 5 | NUM(2)) << 5 | NUM(3);
				break;
			case 7:
				index[0] = ((((NUM(0) << 5 | NUM(4)) << 5 | NUM(8)) << 5 | NUM(1)) << 5 | NUM(9)) << 5 | NUM(10);
				index[1] = ((((NUM(0) << 5 | NUM(4)) << 5 | NUM(5)) << 5 | NUM(6)) << 5 | NUM(7)) << 5 | NUM(11);
				index[2] = ((((NUM(8) << 5 | NUM(12)) << 5 | NUM(9)) << 5 | NUM(10)) << 5 | NUM(14)) << 5 | NUM(11);
				index[3] = ((((NUM(2) << 5 | NUM(14)) << 5 | NUM(3)) << 5 | NUM(7)) << 5 | NUM(11)) << 5 | NUM(15);
				break;
		}
			
		/*
			01246A 0159DE 236ABE 8BCDEF
			37B2A9 376548 BFA9D8 1D048C
			FEDB95 FEA621 DC9541 743210
			C84D56 C89AB7 405627 E2FB73

			----------左右鏡射----------
			
			321759 326AED 10598D B8FEDC
			FB7E65 FBA984 736514 D1C840
			CDE8A6 CD9512 EFA672 470123
			04819A 04567B 8C9AEB 2E37BF
		*/
	}
	float get_tuple(State state[4][4], const int next_tile, const int dir)
	{
		float value = 0;
		#pragma omp parallel for num_threads(8)
		for_i(8)
		{
			unsigned index[4];
			int tmp_dir = dir;
			Index *p;
			if((i & 4) && (dir & 1)) tmp_dir ^= 1;
			tmp_dir += i & 3;
			tmp_dir &= 3;
			tuple_index(index, i);
			for_j(4)
			{
				p = std::lower_bound(Tuple[next_tile][tmp_dir][j], Tuple[next_tile][tmp_dir][j] + num_Tuple[next_tile][tmp_dir][j], index[j]);
				#pragma omp critical
				{
					if(p != Tuple[next_tile][tmp_dir][j] + num_Tuple[next_tile][tmp_dir][j] && p->index == index[j]) value += p->value;
					else if(state && state[tmp_dir][j].find(index[j]) != state[tmp_dir][j].end()) value += state[tmp_dir][j][index[j]];
				}
			}
		}
		return value;
	}
	float tuple_modify(State state[4][4], const float score, const int next_tile, const int dir) //for learning
	{
		float value = 0;
		#pragma omp parallel for num_threads(8)
		for_i(8)
		{
			unsigned index[4];
			int tmp_dir = dir;
			Index *p;
			if((i & 4) && (dir & 1)) tmp_dir ^= 1;
			tmp_dir += i & 3;
			tmp_dir &= 3;
			tuple_index(index, i);
			for_j(4)
			{
				p = std::lower_bound(Tuple[next_tile][tmp_dir][j], Tuple[next_tile][tmp_dir][j] + num_Tuple[next_tile][tmp_dir][j], index[j]);
				#pragma omp critical
				{
					if(p != Tuple[next_tile][tmp_dir][j] + num_Tuple[next_tile][tmp_dir][j] && p->index == index[j]) value += p->value += score;
					else if(state && state[tmp_dir][j].find(index[j]) != state[tmp_dir][j].end()) value += state[tmp_dir][j][index[j]] += score;
					else value += state[tmp_dir][j][index[j]] = score;		
				}
			}
		}
		return value;
	}
	/*friend std::ostream& operator<<(std::ostream &out, const Board board)
	{
		out << "-------\n";
		ULL tmp = board.board;
		for_i(4)
		{
			for_j(4) {out << (tmp & 15) << " "; tmp >>= 4;}
			out << "\n-------\n";
		}
		out << "\n";
		return out;
	}*/
};
