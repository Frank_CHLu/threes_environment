#pragma once
#include <cstring>
class Bag
{
    unsigned tile[5], maxtile;
    bool bonus[12];
public:
    void init()
    {
        std::memset(tile, 0, sizeof(tile));
        std::memset(bonus, 0, sizeof(bonus));
        maxtile = 3;
        tile[1] = tile[2] = tile[3] = 4;
    }
    bool rest_tile(int n) const
    {
        if(n < 4) return tile[n];
        return tile[4] && bonus[n-4];
    }
    void use(int n)
    {
        if(n < 4)
        {
            tile[n]--;
            if(!(tile[1] + tile[2] + tile[3])) tile[1] = tile[2] = tile[3] = 4;
        }
        else
        {
            bool reset = true;
            tile[4]--;
            bonus[n-4] = false;
            for(int i = 0 ; i < maxtile - 6 ; i++) if(bonus[i]) {reset = false; break;}
            if(reset) for(int i = 0 ; i < maxtile - 6 ; i++) bonus[i] = true;
        }
        tile[0]++;
        if(tile[0] % 21 == 20) tile[4]++;
    }
    void reset_max(int n)
    {
        if(n > maxtile && n > 6)
        {
            maxtile = n;
            std::memset(bonus, 0, sizeof(bonus));
            for(int i = 0 ; i < maxtile - 6 ; i++) bonus[i] = true;
        }
    }
    int total() {return tile[0];}
};